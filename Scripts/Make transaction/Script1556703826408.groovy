import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Check that User can add products into cart first via "Add product into cart" test case'
WebUI.callTestCase(findTestCase('Add product into cart'), [('username') : 'user', ('encryptedPassword') : '1/VWEm4uipk='], 
    FailureHandling.STOP_ON_FAILURE)

'Enter cart page'
WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Carts_Menu'))

'Click confirm button'
WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Confirm_Transaction_Button'))

'Accept transaction popup'
WebUI.acceptAlert()

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Transaction_Added_Alert'), 'Well done! You successfully added the transaction.')

