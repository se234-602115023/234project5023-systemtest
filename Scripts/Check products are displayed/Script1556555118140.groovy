import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

'Check that logging in is available'
WebUI.callTestCase(findTestCase('Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

'Find the app product list display'
WebElement productList = driver.findElement(By.xpath('/html/body/app-root/app-product-list'))

'Find the amount of products in the list'
List<WebElement> products = productList.findElements(By.className('farmer-card'))

'Print out the amount of products'
println('No. of rows: ' + products.size())

'Verify the value is correct'
WebUI.verifyEqual(availableProducts, products.size())

'Verify the product is the same as the one in the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Garden'), findTestData('Product Data').getValue(
        1, 1))

'Verify the product is the same as the one in the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Banana'), findTestData('Product Data').getValue(
        1, 2))

'Verify the product is the same as the one in the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Orange'), findTestData('Product Data').getValue(
        1, 3))

'Verify the product is the same as the one in the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Papaya'), findTestData('Product Data').getValue(
        1, 4))

'Verify the product is the same as the one in the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Rambutan'), findTestData('Product Data').getValue(
        1, 5))

