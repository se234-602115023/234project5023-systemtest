import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Check that Admin can log in first via "Login as Admin" test case'
WebUI.callTestCase(findTestCase('Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

'Log out from the website'
WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Logout_Button'))

'Check that the login form is visible on the login page'
WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/Page_Login/login_form'), FailureHandling.STOP_ON_FAILURE)

