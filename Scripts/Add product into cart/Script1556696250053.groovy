import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://52.23.182.15:8085/')

'Enter a given username'
WebUI.setText(findTestObject('Page_ProjectBackend/Page_Login/Username_Field'), username)

'Enter a given password'
WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/Page_Login/Password_Field'), encryptedPassword)

'Log in into the website'
WebUI.click(findTestObject('Page_ProjectBackend/Page_Login/Login_Button'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Products_Menu'), 'Products')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Carts_Menu'), 'Carts')

'Check if the button text is "add to cart"'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Garden_Add_Button'), 'add to cart')

'Add product 1 (Garden) into cart'
WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Product_Garden_Add_Button'))

'Check if the button changed to "already added"'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Garden_Add_Button'), 'already added')

'Check if the amount indicator is 1'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Amount_Indicator'), '1')

'Check if the button text is "add to cart"'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Banana_Add_Button'), 'add to cart')

'Add product 1 (Banana) into cart'
WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Product_Banana_Add_Button'))

'Check if the button changed to "already added"'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Banana_Add_Button'), 'already added')

'Check if the amount indicator is 2'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Product_Amount_Indicator'), '2')

