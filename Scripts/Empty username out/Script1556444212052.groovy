import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://52.23.182.15:8085/')

'Enter a username'
WebUI.sendKeys(findTestObject('Page_ProjectBackend/Page_Login/Username_Field'), 'user')

'Enter a password'
WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/Page_Login/Password_Field'), '1/VWEm4uipk=')

'Ctrl+a on the password field'
WebUI.sendKeys(findTestObject('Page_ProjectBackend/Page_Login/Username_Field'), Keys.chord(Keys.CONTROL, 'a'))

'Delete the hilighted username (WEBUI.ClearText doesn\'t appear to actually remove the string from the field.)'
WebUI.sendKeys(findTestObject('Page_ProjectBackend/Page_Login/Username_Field'), Keys.chord(Keys.DELETE))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Login/Username_Is_Required_Label'), 'Username is required')

