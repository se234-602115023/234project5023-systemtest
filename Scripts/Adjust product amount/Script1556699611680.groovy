import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

'Check beforehand that we can add products into cart fist via "Add product into cart" test case'
WebUI.callTestCase(findTestCase('Add product into cart'), [('username') : 'user', ('encryptedPassword') : '1/VWEm4uipk='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Carts_Menu'))

'Check that garden price is actually 20,000 THB'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Cart_Garden_Price'), '20,000 THB')

'Check that garden price is actually 150 THB'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Cart_Banana_Price'), '150 THB')

'Change the garden amount'
WebUI.setText(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Cart_Garden_Input'), amountGarden)

'Change the banana amount'
WebUI.setText(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Cart_Banana_Input'), amountBanana)

'Check that the total price is the price of all the two products together'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_Cart/Cart_Total_Price'), totalPrice)

