import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Check that Admin can login first via "Login as Admin" test case'
WebUI.callTestCase(findTestCase('Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ProjectBackend/Page_Product/Transactions_Menu'), FailureHandling.STOP_ON_FAILURE)

'Check that the header "Transaction List exists"'
WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_List_Header'), 0)

'Check that the transaction table exists'
WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Table'), 0)

'Check that ID column of the transaction table exists'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_ID_Col'), 'Transaction ID')

'Check that products column of the transaction table exists'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Product_Col'), 'Products')

'Check that amount column of the transaction table exists'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Amount_Col'), 'Amount')

'Check the the given transactionId of the first transaction is correct according to the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Test_One_ID'), TransactionIdOne)

'Check the the given product of the first transaction is correct according to the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Test_One_Product'), TransactionProductOne)

'Check the the given amount of the first transaction is correct according to the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Test_One_Amount'), TransactionAmountOne)

'Check the the given transactionId of the second transaction is correct according to the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Test_Two_ID'), TransactionIdTwo)

'Check the the given product of the second transaction is correct according to the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_Test_Two_Product'), TransactionProductTwo)

'Check the the given amount of the second transaction is correct according to the test data'
WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_Product/Page_TransactionList/Transaction_test_Two_Amount'), TransactionAmountTwo)

