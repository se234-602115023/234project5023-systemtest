<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Logout as User test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b130fa74-c05f-472c-8c2c-50bf51962eb2</testSuiteGuid>
   <testCaseLink>
      <guid>fa9e8106-23fa-4f3b-b7f5-11de58222a7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout as User</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>96204593-cbc5-4afe-96bc-396bcd5e4be1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/User Accounts</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>96204593-cbc5-4afe-96bc-396bcd5e4be1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ff4cb0d8-d4c0-4078-9bed-598e90844ced</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>96204593-cbc5-4afe-96bc-396bcd5e4be1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>encryptedPassword</value>
         <variableId>684ce1dd-2b1f-40ef-908e-e52d71f0b668</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
