<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login as User test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>72e3a7bc-7d3e-4a0d-9217-a3ed1ac8bd22</testSuiteGuid>
   <testCaseLink>
      <guid>6a6a9818-026f-416a-8119-fd637563df1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login as User</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>85363558-cdc7-4d4c-915a-71225751ad6e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/User Accounts</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>85363558-cdc7-4d4c-915a-71225751ad6e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ff4cb0d8-d4c0-4078-9bed-598e90844ced</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>85363558-cdc7-4d4c-915a-71225751ad6e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>encryptedPassword</value>
         <variableId>684ce1dd-2b1f-40ef-908e-e52d71f0b668</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
