<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Logout cases test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>52a34fc3-a910-4eea-9bbc-9b5de917134a</testSuiteGuid>
   <testCaseLink>
      <guid>3e490ac3-2d0a-49f5-a5ab-40a71c4553d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout as User</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0a0195f8-bddc-4e3e-a3ac-234035015145</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/User Accounts</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0a0195f8-bddc-4e3e-a3ac-234035015145</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ff4cb0d8-d4c0-4078-9bed-598e90844ced</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0a0195f8-bddc-4e3e-a3ac-234035015145</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>encryptedPassword</value>
         <variableId>684ce1dd-2b1f-40ef-908e-e52d71f0b668</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d540e456-5765-4923-8477-5ba2ebee75e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout as Admin</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
