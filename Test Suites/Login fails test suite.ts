<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login fails test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0c47e744-bfbf-4dd1-ace1-8390e0642c63</testSuiteGuid>
   <testCaseLink>
      <guid>78da0a05-bb98-4f1a-a1e9-44670dc0a7e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login fails</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c97c94f3-fb80-4ba0-a0cc-218f672501a0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Invalid User Accounts</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c97c94f3-fb80-4ba0-a0cc-218f672501a0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>f7414bdf-d6a9-4803-8151-0bdf305952f2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c97c94f3-fb80-4ba0-a0cc-218f672501a0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>encryptedPassword</value>
         <variableId>7003e1cc-c424-4c00-95c6-4c832f453e10</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
