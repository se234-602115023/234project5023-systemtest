<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Adjust product amount test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4e775dc0-56b9-4ec0-b8f9-189c12a979a1</testSuiteGuid>
   <testCaseLink>
      <guid>d5d2da92-dd48-45f4-b9d5-fffbefa7df2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Adjust product amount</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a1df02ec-e9da-4d89-a01a-9e9ba60f4d5f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Product Amount Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>a1df02ec-e9da-4d89-a01a-9e9ba60f4d5f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>amountGarden</value>
         <variableId>c4303ffa-0ff7-4033-8d39-fc7e15b51d47</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a1df02ec-e9da-4d89-a01a-9e9ba60f4d5f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>amountBanana</value>
         <variableId>d9db8b6f-d8c5-4343-88eb-ed189cccde39</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a1df02ec-e9da-4d89-a01a-9e9ba60f4d5f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>expectedPrice</value>
         <variableId>056ce27c-bd75-4ea8-96f4-c892b09af8d0</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
